import 'package:calculator/functions/therme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Sism extends StatefulWidget {
  Sism({super.key, required this.title});

  final String title;

  @override
  State<Sism> createState() => _Sism();
}

class _Sism extends State<Sism> {
  int _selectedIndex = 0;
  final TextEditingController pulgadas = TextEditingController();
  final TextEditingController pies = TextEditingController();
  final TextEditingController yardas = TextEditingController();
  final TextEditingController millas = TextEditingController();
  final TextEditingController millas_nautica = TextEditingController();
  double cmd = 0.0; // Nueva variable agregada
  double cmp = 0.0; // Nueva variable agregada
  double cmy = 0.0; // Nueva variable agregada
  double km = 0.0;
  double kmm = 0.0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Formulas Therme = Formulas();
  @override
  void dispose() {
    pulgadas.dispose();
    pies.dispose();
    yardas.dispose();
    millas.dispose();
    millas_nautica.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Expanded(
              child: Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Pulgada"),
                          Container(
                              margin: EdgeInsets.all(10),
                              child: TextField(
                                controller: pulgadas,
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'^\d+\.?\d{0,1}'))
                                ],
                              )),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Pie"),
                          Container(
                            margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: pies,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Yarda"),
                          Container(
                            margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: yardas,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Milla"),
                          Container(
                            margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: millas,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Milla Nautica"),
                          Container(
                            margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: millas_nautica,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                    ],
                  )
                ),
              Expanded(
                flex: 1,
                child: Column(
                    children: [
                      Padding(padding: EdgeInsets.all(5)),
                      ElevatedButton(
                          onPressed: () {
                            double Pulgada =
                                double.tryParse(pulgadas.text) ?? 0.0;
                            double yarda = double.tryParse(yardas.text) ?? 0.0;
                            double pie = double.tryParse(pies.text) ?? 0.0;
                            double milla = double.tryParse(millas.text) ?? 0.0;
                            double milla_nautica =
                                double.tryParse(millas_nautica.text) ?? 0.0;
                            setState(() {
                              cmd = Therme.pulgadas(Pulgada) as double;
                              cmp = Therme.pie(yarda) as double;
                              cmy = Therme.yarda(pie) as double;
                              km = Therme.milla(milla) as double;
                              kmm =
                                  Therme.milla_nautica(milla_nautica) as double;
                            });
                          },
                          child: Text("CALCULAR")),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("PULGADAS $cmd CM")],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("YARDAS $cmy CM")],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("PIE $cmp CM")],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("MILLA: $km KM")],
                      )),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("MILLA NAUTICA: $kmm KM")],
                      )),
                    ],
                  )
                ),
            ],
          ))
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.calculate),
              label: 'Longitud',
              backgroundColor: Colors.red),
          BottomNavigationBarItem(
              icon: Icon(Icons.monitor_weight),
              label: 'Masa',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.water_drop),
              label: 'Capacidad',
              backgroundColor: Colors.red),
        ],
        onTap: (int index) {
          switch (index) {
            case 0:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/sistema_medicion');
              break;
            case 1:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/masa');
              break;
            case 2:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/liquid');
              break;
          }
        },
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/calculator.png"),
                      fit: BoxFit.fitHeight)),
              child: Text("Calculator Plus"),
            ),
            ListTile(
              title: const Text('Calculator'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calc');
              },
            ),
            ListTile(
              title: const Text('Physics'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/sistema_medicion');
              },
            ),
            ListTile(
              title: const Text('Thermo'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/thermo');
              },
            )
          ],
        ),
      ),
    );
  }
}
