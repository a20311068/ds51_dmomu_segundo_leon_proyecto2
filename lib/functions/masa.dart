import 'package:flutter/material.dart';

class Masa extends StatefulWidget {
  Masa({super.key, required this.title});

  final String title;
  @override
  _WeightConverterState createState() => _WeightConverterState();
}

class _WeightConverterState extends State<Masa> {
  int _selectedIndex = 0;
  final TextEditingController _ouncesController = TextEditingController();
  final TextEditingController _poundsController = TextEditingController();
  double _gramsFromOunces = 0;
  double _gramsFromPounds = 0;

  @override
  void dispose() {
    _ouncesController.dispose();
    _poundsController.dispose();
    super.dispose();
  }

  void _convertFromOunces() {
    setState(() {
      double ounces = double.tryParse(_ouncesController.text) ?? 0;
      _gramsFromOunces = ounces * 28.35;
    });
  }

  void _convertFromPounds() {
    setState(() {
      double pounds = double.tryParse(_poundsController.text) ?? 0;
      _gramsFromPounds = pounds * 453.59;
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: _ouncesController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Onzas',
                hintText: 'Valor en onzas',
              ),
              onChanged: (_) => _convertFromOunces(),
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: _poundsController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Libras',
                hintText: 'Valor en libras',
              ),
              onChanged: (_) => _convertFromPounds(),
            ),
            SizedBox(height: 32.0),
            Text(
              '${_ouncesController.text} Onzas = $_gramsFromOunces g.',
              style: TextStyle(fontSize: 20.0),
            ),
            SizedBox(height: 16.0),
            Text(
              '${_poundsController.text} Libras  = $_gramsFromPounds g.',
              style: TextStyle(fontSize: 20.0),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.calculate_rounded),
              label: 'Longitud',
              backgroundColor: Colors.red),
          BottomNavigationBarItem(
              icon: Icon(Icons.monitor_weight),
              label: 'Masa',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.water_drop),
              label: 'Capacidad',
              backgroundColor: Colors.red),
        ],
        currentIndex: _selectedIndex,
        onTap: (int index) {
          switch (index) {
            case 0:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/sistema_medicion');
              break;
            case 1:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/masa');
              break;
            case 2:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/liquid');
              break;
          }
        },
      ),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/calculator.png"),
                      fit: BoxFit.fitHeight)),
              child: Text("Calculator Plus"),
            ),
            ListTile(
              title: const Text('Calculator'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calc');
              },
            ),
            ListTile(
              title: const Text('Physics'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/sistema_medicion');
              },
            ),
            ListTile(
              title: const Text('Thermo'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/thermo');
              },
            )
          ],
        ),
      ),
    );
  }
}
