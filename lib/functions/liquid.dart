import 'package:flutter/material.dart';

class Liquid extends StatefulWidget {
  Liquid({super.key, required this.title});

  final String title;

  @override
  State<Liquid> createState() => _LiquidState();
}

class _LiquidState extends State<Liquid> {
  int _selectedIndex = 0;
  final TextEditingController _gallonsController = TextEditingController();
  final TextEditingController _fluidOuncesController = TextEditingController();
  double _litersFromGallons = 0;
  double _litersFromFluidOunces = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void dispose() {
    _gallonsController.dispose();
    _fluidOuncesController.dispose();
    super.dispose();
  }

  void _convertFromGallons() {
    setState(() {
      double gallons = double.tryParse(_gallonsController.text) ?? 0;
      double totalFluidOunces = gallons * 128;
      _litersFromGallons = totalFluidOunces / 33.814;
    });
  }

  void _convertFromFluidOunces() {
    setState(() {
      double fluidOunces = double.tryParse(_fluidOuncesController.text) ?? 0;
      _litersFromFluidOunces = fluidOunces / 33.814;
    });
  }

  double get _gramsFromLitersGallons {
    return _litersFromGallons * 1000;
  }

  double get _gramsFromLitersFluidOunces {
    return _litersFromFluidOunces * 1000;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: _gallonsController,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                labelText: 'Galones',
                hintText: 'Valor en Galones',
              ),
            ),
            ElevatedButton(
              onPressed: _convertFromGallons,
              child: const Text('Convertir a lt'),
            ),
            const SizedBox(height: 16.0),
            Text(
              '$_litersFromGallons Lt = ${_gramsFromLitersGallons.toStringAsFixed(2)} g',
              style: const TextStyle(fontSize: 24.0),
            ),
            const SizedBox(height: 32.0),
            TextField(
              controller: _fluidOuncesController,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                labelText: 'Onzas liquidas',
                hintText: 'Valor de onzas liquidas',
              ),
            ),
            ElevatedButton(
              onPressed: _convertFromFluidOunces,
              child: const Text('Convertir a lt'),
            ),
            const SizedBox(height: 16.0),
            Text(
              '$_litersFromFluidOunces lt = ${_gramsFromLitersFluidOunces.toStringAsFixed(2)} g',
              style: const TextStyle(fontSize: 24.0),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.calculate),
              label: 'Longitud',
              backgroundColor: Colors.red),
          BottomNavigationBarItem(
              icon: Icon(Icons.monitor_weight),
              label: 'Masa',
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.water_drop),
              label: 'Capacidad',
              backgroundColor: Colors.red),
        ],
        currentIndex: _selectedIndex,
        onTap: (int index) {
          switch (index) {
            case 0:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/sistema_medicion');
              break;
            case 1:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/masa');
              break;
            case 2:
              selectedItemColor:
              Colors.amber[800];
              _onItemTapped(index);
              Navigator.pushNamed(context, '/functions/liquid');
              break;
          }
        },
      ),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/calculator.png"),
                      fit: BoxFit.fitHeight)),
              child: Text("Calculator Plus"),
            ),
            ListTile(
              title: const Text('Calculator'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calc');
              },
            ),
            ListTile(
              title: const Text('Physics'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/sistema_medicion');
              },
            ),
            ListTile(
              title: const Text('Thermo'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/thermo');
              },
            )
          ],
        ),
      ),
    );
  }
}
