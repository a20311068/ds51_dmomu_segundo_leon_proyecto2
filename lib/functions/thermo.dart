import 'package:flutter/material.dart';

class Thermo extends StatefulWidget {
  Thermo({super.key, required this.title});

  final String title;

  @override
  State<Thermo> createState() => _ThermoState();
}

class _ThermoState extends State<Thermo> {
  final farent = TextEditingController();
  final celcius = TextEditingController();
  final kelvin = TextEditingController();
  double f = 0, c = 0, k = 0, a = 0;

  @override
  void dispose() {
    farent.dispose();
    celcius.dispose();
    kelvin.dispose();
    super.dispose();
  }

  void farentheit() {
    setState(() {
      if (farent.text.isEmpty) {
        a = 0;
      } else {
        a = double.parse(farent.text);
      }
      f = a;
      c = (a - 32) * 0.555;
      k = (a + 459.67) * 0.555;
      celcius.text = c.toString();
      kelvin.text = k.toString();
    });
  }

  void celc() {
    setState(() {
      if (celcius.text.isEmpty) {
        a = 0;
      } else {
        a = double.parse(celcius.text);
      }
      f = (a * 1.8) + 32;
      c = a;
      k = a - 273.15;
      farent.text = f.toString();
      kelvin.text = k.toString();
    });
  }

  void kelv() {
    setState(() {
      if (kelvin.text.isEmpty) {
        a = 0;
      } else {
        a = double.parse(kelvin.text);
      }
      f = (a * 1.8) + 459.67;
      c = a - 273.15;
      k = a;
      farent.text = f.toString();
      celcius.text = c.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        widget.title,
        style: const TextStyle(color: Colors.white),
      )),
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(30, 20, 30, 20),
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(20),
                ),
                const Text(
                  "Cálculo de Temperatura",
                  style: TextStyle(fontSize: 30),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                ),
                TextField(
                  decoration: InputDecoration(
                    label: const Text("Fahrenheit"),
                    focusColor: Colors.blue,
                    hintText: 'Ingrese los grados en Fahrenheit',
                    labelStyle: const TextStyle(fontSize: 30),
                    border: myinputborder(),
                    enabledBorder: myinputborder(),
                    focusedBorder: myinputborder(),
                  ),
                  controller: farent,
                  onChanged: (value) => farentheit(),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                ),
                TextField(
                  decoration: InputDecoration(
                    label: const Text("Celcius"),
                    hintText: 'Ingrese los grados en Celcius',
                    labelStyle: const TextStyle(fontSize: 30),
                    border: myinputborder(),
                    enabledBorder: myinputborder(),
                    focusedBorder: myinputborder(),
                  ),
                  controller: celcius,
                  onChanged: (value) => celc(),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                ),
                TextField(
                  decoration: InputDecoration(
                    label: const Text("Kelvin"),
                    hintText: 'Ingrese los grados en Kelvin',
                    labelStyle: const TextStyle(fontSize: 30),
                    border: myinputborder(),
                    enabledBorder: myinputborder(),
                    focusedBorder: myinputborder(),
                  ),
                  controller: kelvin,
                  onChanged: (value) => kelv(),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                ),
              ],
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/calculator.png"),
                      fit: BoxFit.fitHeight)),
              child: Text('Calculator Plus'),
            ),
            ListTile(
              title: const Text('Calculator'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calc');
              },
            ),
            ListTile(
              title: const Text('Physics'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/medition_sistem');
              },
            ),
            ListTile(
              title: const Text('Thermo'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/thermo');
              },
            )
          ],
        ),
      ),
    );
  }

  OutlineInputBorder myinputborder() {
    return const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      borderSide: BorderSide(
        width: 3,
      )
    );
  }
}
