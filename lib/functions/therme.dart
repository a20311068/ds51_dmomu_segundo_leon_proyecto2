class Formulas{

  pulgadas(pulgadas){
    double cm = pulgadas * 2.54;
    return cm;
  }
  yarda(yarda){
    double cm = yarda * 91.44;
    return cm;

  }
  pie(pie){
    double cm = pie * 30.48;    
    return cm;

  }
  milla(milla){
    double km = milla * 1.60934;    
    return km;

  }
 
  milla_nautica(milla_nautica){
    double km = milla_nautica * 1.8519984;
    return km;
  }
 
}
