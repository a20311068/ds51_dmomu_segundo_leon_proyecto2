import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class Home extends StatefulWidget {
  Home({super.key, required this.title});

  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final txtEntrada = TextEditingController();
  final txtResultado = TextEditingController();
  String strInput = "";

  @override
  void initState() {
    super.initState();
    txtEntrada.addListener(() {});
    txtResultado.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 20, 61, 164),
      appBar: AppBar( 
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          const SizedBox(height: 20.0),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextField(
                decoration: const InputDecoration.collapsed(
                    hintText: " ", hintStyle: TextStyle(fontSize: 40)),
                style: const TextStyle(
                  fontSize: 40,
                ),
                textAlign: TextAlign.left,
                controller: txtEntrada,
                onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              )),
          const SizedBox(height: 100.0),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  txtResultado.text,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                      fontSize: 42, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              btnAC('C', const Color(0xFFF5F7F9)),
              btnBorrar(),
              boton('%', const Color(0xFFF5F7F9)),
              boton('/', const Color(0xFFF5F7F9)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              boton('7', Colors.white),
              boton('8', Colors.white),
              boton('9', Colors.white),
              boton('*', const Color(0xFFF5F7F9))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              boton('4', Colors.white),
              boton('5', Colors.white),
              boton('6', Colors.white),
              boton('-', const Color(0xFFF5F7F9)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              boton('1', Colors.white),
              boton('2', Colors.white),
              boton('3', Colors.white),
              boton('+', const Color(0xFFF5F7F9)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              boton('0', Colors.white),
              boton('.', Colors.white),
              btnIgual(),
            ],
          ),
          const SizedBox(height: 10.0)
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/calculator.png"),
                      fit: BoxFit.fitHeight)),
              child: Text("Calculator Plus"),
            ),
            ListTile(
              title: const Text('Calculator'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calc');
              },
            ),
            ListTile(
              title: const Text('Physics'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/medition_sistem');
              },
            ),
            ListTile(
              title: const Text('Thermo'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/functions/thermo');
              },
            )
          ],
        ),
      ),
    );
  }

  Widget boton(btntxt, Color btnColor) {
    return Container(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: MaterialButton(
        onPressed: () {
          setState(() {
            if (txtResultado.text.isEmpty) {
              if (btntxt == '.' && txtEntrada.text.isEmpty) {
                btntxt = '0.';
              }
              txtEntrada.text = txtEntrada.text + btntxt;
            } else {
              switch (btntxt) {
                case '*':
                  txtEntrada.text = txtResultado.text + btntxt;
                  txtResultado.text = "";
                  break;
                case '/':
                  txtEntrada.text = txtResultado.text + btntxt;
                  txtResultado.text = "";
                  break;
                case '+':
                  txtEntrada.text = txtResultado.text + btntxt;
                  txtResultado.text = "";
                  break;
                case '-':
                  txtEntrada.text = txtResultado.text + btntxt;
                  txtResultado.text = "";
                  break;
                case '%':
                  txtEntrada.text = txtResultado.text + btntxt;
                  txtResultado.text = "";
                  break;
                case '.':
                  txtEntrada.text = txtResultado.text + btntxt;
                  txtResultado.text = "";
                  break;
                default:
                  txtResultado.text = "";
                  txtEntrada.text = btntxt;
              }
            }
          });
        },
        color: btnColor,
        padding: const EdgeInsets.all(15.0),
        splashColor: Colors.black,
        shape: const CircleBorder(),
        child: Text(
          btntxt,
          style: const TextStyle(
            fontSize: 28.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget btnAC(btntext, Color btnColor) {
    return Container(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: MaterialButton(
        onPressed: () {
          setState(() {
            txtEntrada.text = "";
            txtResultado.text = "";
          });
        },
        color: btnColor,
        padding: const EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: const CircleBorder(),
        child: Text(
          btntext,
          style: const TextStyle(
            fontSize: 28.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget btnBorrar() {
    return Container(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: MaterialButton(
        onPressed: () {
          if (txtEntrada.text.isNotEmpty) {
            txtEntrada.text =
                txtEntrada.text.substring(0, txtEntrada.text.length - 1);
          } else {
            txtEntrada.text = "";
          }
        },
        color: const Color(0xFFF5F7F9),
        padding: const EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: const CircleBorder(),
        child: const Icon(Icons.backspace, size: 35, color: Colors.blueGrey),
      ),
    );
  }

  Widget btnIgual() {
    return Container(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: MaterialButton(
        onPressed: () {
          Parser p = Parser();
          ContextModel cm = ContextModel();
          Expression exp = p.parse(txtEntrada.text);
          setState(() {
            txtResultado.text =
                exp.evaluate(EvaluationType.REAL, cm).toString();
          });
        },
        color: Colors.cyan,
        padding: const EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: const CircleBorder(),
        child: const Text('=',
            style: TextStyle(fontSize: 28.0, color: Colors.black)),
      ),
    );
  }
}
