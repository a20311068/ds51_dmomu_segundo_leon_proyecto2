import 'package:calculator/calc.dart';
import 'package:calculator/functions/liquid.dart';
import 'package:calculator/functions/masa.dart';
import 'package:calculator/functions/math.dart';
import 'package:calculator/functions/medition_sistem.dart';
import 'package:calculator/functions/thermo.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculator Plus',
      theme:
          ThemeData(primarySwatch: Colors.blue, brightness: Brightness.light),
      darkTheme: ThemeData(
        primarySwatch: Colors.indigo,
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.system,
      routes: {
        '/calc': (context) => Home(title: 'Calculator Plus'),
        '/functions/math': (context) => const Math(title: 'Math'),
        '/functions/medition_sistem': (context) => const Sism(title: 'Physics'),
        '/functions/thermo': (context) => Thermo(title: 'Thermo'),
        '/functions/masa': (context) => const Masa(title: 'Mass'),
        '/functions/liquid': (context) => const Liquid(title: 'Capabilities'),
      },
      home: Home(title: 'Calculator Plus'),
    );
  }
}
